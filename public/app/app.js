
var tiler = angular.module('tiler', ['ui.router']);

tiler.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider.state('index', {
        url: '/',
        templateUrl: 'app/components/index/index.html'
    });
}]);