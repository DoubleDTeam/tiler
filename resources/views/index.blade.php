<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <script src="assets/libs/jquery-3.1.0.min.js"></script>

        <script src="app/angular.min.js"></script>
        <script src="app/angular-ui-router.min.js"></script>

        <script src="app/app.js"></script>

        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="assets/js/bootstrap.min.js"></script>

    </head>
    <body ng-app="tiler">
        <ui-view></ui-view>
    </body>
</html>
