# Tiler 
[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)

##Description
Tile, this is a new awesome portal for publishing and storing articles.

As well as a greater number of API for integration with other services.

###Technologies
* Frontend built with
    * [AngularJs](https://angularjs.org/)
    * [Bootstrap](http://getbootstrap.com/)
    * [Ui-Router](https://github.com/angular-ui/ui-router)
    * [Font Awesome](http://fontawesome.io/)
* Backend built with
    * [Laravel](https://laravel.com/)
    * [JWT-Auth](https://github.com/tymondesigns/jwt-auth)
    * [Intervention Image](http://image.intervention.io/)   

###Developers
* Frontend developer [Denis Danilov](https://vk.com/id220184427)
* Backend developer [Danila Denisov](https://vk.com/danyadenisov)

##Documentation for API
[REST API map](https://drive.google.com/file/d/0Bx6lIusm32N8c3oyN1hna3JabkU/view?usp=sharing)